var path = {
    'SOURCE': './source/assets/',
    'DESTINATION': './public_html/assets/',
    'BOWER': './source/lib/bower_components/'
}


var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir.config.SOURCE = 'source/';
elixir.config.publicPath = 'public_html';
elixir.config.appPath = 'app';
//elixir.config.viewPath = 'source/views';
elixir.config.publicPath = 'public_html';
elixir.config.sourcemaps = false;
elixir.config.versioning.buildFolder = 'public_html';
// automated vendor prefixes
elixir.config.css.autoprefix.options.browsers = ['> 1%', 'Last 2 versions', 'IE 9'];

elixir(function(mix) {
    // compile sass file
    mix.sass([path.SOURCE + 'sass/common.scss'], path.SOURCE + 'css/common.css');

    // copy css to public path
    mix.copy(path.SOURCE + 'css/', path.DESTINATION + 'css/');

    // copy js file
    mix.copy(path.SOURCE + 'js/', path.DESTINATION + 'js/');

    mix.copy(path.BOWER + 'font-awesome/css/font-awesome.min.css', path.DESTINATION + 'css/font-awesome.min.css');
    mix.copy(path.BOWER + 'font-awesome/fonts', path.DESTINATION + 'fonts');

    //mix.scripts('scripts/app.js', path.DESTINATION + '/scripts/app.min.js', elixir.config.publicPath);

});
