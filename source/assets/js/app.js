/**
 * Name : Simple Slider JS
 * Author : Abdul Kader
 * Source : git source
 * Date : 22-10-2016
 */

// slider object
var mySlider = {};
var sliderContent, childCount, btnNext, btnPrev,
    currentItem = 0,
    offsetWidth = 0,
    sliderContentWidth = 0,
    $elem,
    $this,
    paginationItems = '',
    animationDuration = 1200,
    animating = false;

//var jsonPATH = 'https://www.propertyfinder.ae/en/find-broker/ajax/search?page=1';
var jsonPATH = 'assets/json/search.json',
    htmlContent = '',
    sliderElem = document.getElementById('slider-1'),
    sliderContent = sliderElem.getElementsByClassName('slider-content')[0],
    timerInterval = 4000,
    autoSlide = false,
    btnNextClassName = 'btn-next',
    btnPrevClassName = 'btn-prev',
    pagination = sliderElem.getElementsByClassName('slider-pagination')[0],
    paginationLink = document.querySelectorAll('ul.slider-pagination li a');
/**
 * Fetch Remote JSON Data
 * @param  {string}   path     URL to JSON Data Object
 * @param  {Function} callback function to execute after fetching
 * @return {json}            data received wil be available as json in the callback
 */
mySlider.fetchJSON = function(path, callback) {
    var httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === 4) {
            if (httpRequest.status === 200) {
                var data = JSON.parse(httpRequest.responseText);
                if (callback) callback(data);
            }
        }
    };
    httpRequest.open('GET', path);
    httpRequest.send();
};

/**
 * update the slider width
 * @param  {string} slider element
 */
mySlider._update = function(elem) {
    $elem = elem;
    $this = mySlider;
    sliderContent = $elem.getElementsByClassName('slider-content')[0];
    pagination = sliderElem.getElementsByClassName('slider-pagination')[0];
    childCount = sliderContent.children.length;
    for (var i = 0; i < childCount; i++) {
        paginationItems += '<li><a href="#"></a></li>';
    }
    pagination.innerHTML = paginationItems;
};

mySlider._updatePagination = function(currentItem) {
    document.querySelectorAll('ul.slider-pagination li a').forEach(a => a.classList.remove('active'));
    pagination.childNodes[currentItem].children[0].classList.add('active');
}
mySlider._slideNext = function(e) {
    if (animating) {
        return;
    }
    animating = true;
    // hide current slide
    sliderContent.children[currentItem].classList.remove("active", 'left', 'right');
    // increment the counter
    currentItem++;
    // reset the counter if its the last item
    if (currentItem >= (childCount)) {
        currentItem = 0;
    }
    // make new slide visible
    sliderContent.children[currentItem].classList.add("active", 'right');
    $this._updatePagination(currentItem);
    setTimeout(function() {
        animating = false;
    }, animationDuration);
};

mySlider._slidePrev = function(e) {
    if (animating) {
        return;
    }
    animating = true;
    // hide current slide
    sliderContent.children[currentItem].classList.remove("active", 'left', 'right');
    // increment the counter
    currentItem--;
    // reset the counter if its the last item
    if (currentItem < 0) {
        currentItem = childCount - 1;
    }
    // make new slide visible
    sliderContent.children[currentItem].classList.add("active", 'left');
    $this._updatePagination(currentItem);
    setTimeout(function() {
        animating = false;
    }, animationDuration);
};

mySlider._autoSlide = function(e) {
    setInterval(function() {
        mySlider._slideNext(e);
    }, timerInterval);
};

mySlider._slideTo = function(index) {
    if (animating) {
        return;
    }
    animating = true;
    sliderContent.children[currentItem].classList.remove("active", 'left', 'right');
    if (index > currentItem) {
        currentItem = index;
        sliderContent.children[currentItem].classList.add("active", 'right');
    } else {
        currentItem = index;
        sliderContent.children[currentItem].classList.add("active", 'left');
    }
    setTimeout(function() {
        animating = false;
    }, animationDuration);
};
/**
 * Initialize Slider 
 * @param  {string} elem HTML element
 */
mySlider.init = function(elem) {
    $elem = elem;
    $this = mySlider;
    sliderContent = $elem.getElementsByClassName('slider-content')[0];
    childCount = sliderContent.children.length;
    btnNext = $elem.getElementsByClassName(btnNextClassName)[0];
    btnPrev = $elem.getElementsByClassName(btnPrevClassName)[0];

    // check if slides exists
    if (childCount > 0) {
        // update the slider sizes
        $this._update(elem);

        sliderContent.children[0].classList.add("active");
        $this._updatePagination(0);

        btnNext.onclick = function(e) {
            $this._slideNext(e);
            e.preventDefault();
        };

        btnPrev.onclick = function(e) {
            $this._slidePrev(e);
            e.preventDefault();
        };

        paginationLink.onclick = function(e) {
            console.log(e, 1);
        };

        document.querySelectorAll('ul.slider-pagination li a').onclick = function(e) {
            console.log(e);
        };

        if (autoSlide) {
            $this._autoSlide();
        }
    }
};

document.addEventListener('DOMContentLoaded', function(e) {
    // fetch JSON data via AJAX
    mySlider.fetchJSON(jsonPATH, function(data) {
        // verify if ajax data has expected values
        if (data.data.length) {
            // loop through the data and append to slider content
            var clientLists = data.data;

            for (var i = 0; i < clientLists.length; i++) {

                var clientListItem = clientLists[i];

                htmlContent += '<div class="slider-item">' +
                    '<div class="agent-info">' +
                    '<div class="table">' +
                    '<div class="cell">' +
                    '<a href="#" class="agent-logo"><img src="' + clientListItem.links.logo + '" alt=""></a>' +
                    '<!-- /.agent-logo -->' +
                    '<h3><a href="#">' + clientListItem.name + '</a></h3>' +
                    '<h4><span class="fa fa-map-marker"></span> ' + clientListItem.location + '</h4>' +
                    '<h4><span class="fa fa-phone"></span> ' + clientListItem.phone + '</h4>' +
                    '<!-- <div class="description">' +
                    '<p>' + clientListItem.description + '</p>' +
                    '</div> -->' +
                    '<!-- /.description -->' +
                    '</div>' +
                    '<!-- /.cell -->' +
                    '</div>' +
                    '<!-- /.table -->' +
                    '<ul class="tab-list">' +
                    '<li>' +
                    '<span class="label">' + clientListItem.licenseLabel + '</span>' +
                    '<span class="details">' + clientListItem.licenseNumber + '</span>' +
                    '</li>' +
                    '<li>' +
                    '<span class="label">ACTIVE LISTINGS</span>' +
                    '<span class="details">' + clientListItem.totalProperties + '</span>' +
                    '</li>' +
                    '<li>' +
                    '<span class="label">EMPLOYEES</span>' +
                    '<span class="details">' + clientListItem.agentCount + '</span>' +
                    '</li>' +
                    '</ul>' +
                    '<!-- /.tab-list -->' +
                    '</div>' +
                    '<!-- /.agent-info -->' +
                    '<div class="slider-bg" style="background-image:url(\'' + clientListItem.links.logo + '\');">' +
                    '</div>' +
                    '<!-- /.slider-bg -->' +
                    '</div>' +
                    '<!-- /.slider-item -->';
            }

            sliderContent.innerHTML = htmlContent;
            mySlider.init(sliderElem);
        };
    });
});
